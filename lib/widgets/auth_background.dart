import 'package:flutter/material.dart';

class AuthBackground extends StatelessWidget {
  const AuthBackground({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.red,
      width: double.infinity,
      height: double.infinity,
      child: Stack(
        children: [
          _PurpleBox(),
        ],
      ),
    );
  }
}

class _PurpleBox extends StatelessWidget {
  
  

  @override
  Widget build(BuildContext context) {

    final size = MediaQuery.of(context).size;

    return Container(
      width: double.infinity,
      height: size.height * 0.4,
      // color: Colors.indigo,
      decoration: purpleBoxDecoration(),
      child: Stack(
        children: [
          Positioned(
            child: _Bubble(),
            top: -10,
            left: size.width*0.10,
            ),
           
             Positioned(
            child: _Bubble(),
            bottom: 10,
            left: size.width*0.50,
            ),
             Positioned(
            child: _Bubble(),
            bottom: 50,
            left: size.width*0.04,
            ),
           
              Positioned(
            child: _Bubble(),
            right: 15,
            top: -20,
            ),
           
                Positioned(
            child: _Bubble(),
            top: 90,
            left: 210,
            ),
        ],
      ),
    );
  }

  BoxDecoration purpleBoxDecoration() => BoxDecoration(
    gradient: LinearGradient(
      colors: [
        Color.fromARGB(255, 41, 41, 139),
        Color.fromRGBO(90, 70, 158, 1.0),
      ]
    ),
  );
}


class _Bubble extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100.0,
      height: 100.0,
      decoration: BoxDecoration(
        color: Color.fromRGBO(255, 255, 255, 0.03),
        borderRadius: BorderRadius.circular(100),
      ),
    );
  }
}